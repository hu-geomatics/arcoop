from os.path import join
from lamos.datacube.types import MGRSFootprintCollection
from lamos.datacube.jobs import *
from hub.timing import tic, toc

def doit():

    rootDC = r'G:\_EnMAP\Rohdaten\Brazil\Raster\LANDSAT\datacubePESA'
    dir_landsat = join(rootDC, 'landsat')
    dir_aux = join(rootDC, 'auxiliary')
    dir_landsatSample = join(rootDC, 'landsatSample')

    mgrsFootprints = MGRSFootprintCollection(['22LCH'])

    job_sample(indirname=dir_landsat, outdirname=dir_landsatSample,
               locdirname=dir_aux, locproductname='samples', locimagename='mask',
               mgrsFootprints=mgrsFootprints,
               processes=50, skip=0)

if __name__ == '__main__':

    tic()
    doit()
    toc()