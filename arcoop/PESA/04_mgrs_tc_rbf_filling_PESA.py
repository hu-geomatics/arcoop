from os.path import join
from hub.datetime import DateRange, Date
from lamos.datacube.applier import applierControls
from lamos.datacube.types import MGRSFootprintCollection
from arcoop.debug_rbf.job_landsat_timeseries_rbf_gap_filling import *


def doit():

    rootDC = r'G:\_EnMAP\Rohdaten\Brazil\Raster\LANDSAT\datacubePESA'

    dir_landsat = join(rootDC, 'landsat')
    dir_product = join(rootDC, 'productMGRS_PESA_32')

    mgrsFootprints = MGRSFootprintCollection(['22LCH'])



    dateRange = DateRange(start=Date(2014, 1, 1), end=Date(2015, 12, 31))
    stepFit = 8  # fit on 5-days bins
    stepPredict = 1  # predict on (5*2=)10-days bins

    stddevs = [32]
    wsize = 64
    controls = applierControls(blockxsize=wsize, blockysize=wsize, wsize=wsize)
    job_landsat_timeseries_rbf_gap_filling(indirname=dir_landsat, outdirname=dir_product,
                                                stddevs=stddevs,
                                                outlierFlt=True,
                                                outlierStd=20,
                                                stdTimes=1,
                                                vegIndex='evi',
                                                mgrsFootprints=mgrsFootprints,
                                                productname='timeseries', imagename='evi',
                                                dateRange=dateRange,
                                                stepFit=stepFit, stepPredict=stepPredict,
                                                controls=controls,
                                                processes=20, skip=False)

if __name__ == '__main__':
    from osgeo import gdal
    gdal.SetCacheMax(1)
    doit()