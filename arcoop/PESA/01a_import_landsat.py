from os.path import join
from lamos.datacube.types import MGRSFootprintCollection, WRS2FootprintCollection, LANDSAT_ANCHOR
from lamos.datacube.jobs import *
from hub.timing import tic, toc
from lamos.datacube.applier import applierControls

def doit():
    root = r'G:\_EnMAP\Rohdaten\Brazil\Raster\LANDSAT'
    rootDC = join(root, 'datacubePESA_Hugo')
    dir_landsatWRS2 = join(root, 'ESPA\PESA')
    dir_landsat = join(rootDC, 'landsat')
    dir_products = join(rootDC, 'products')

    resolution = 30
    buffer = 300
    anchor = LANDSAT_ANCHOR

    wrs2Footprints = WRS2FootprintCollection(['224071', '224071'])
    mgrsFootprints = MGRSFootprintCollection(['22LCH'])

    # import landsat scenes into data cube
    job_import_landsat_archive(indirname=dir_landsatWRS2, outdirname=dir_landsat,
                               skipTOABT=True,
                               wrs2Footprints=wrs2Footprints,
                               mgrsFootprints=mgrsFootprints,
                               resolution=resolution, buffer=buffer, anchor=anchor,
                               saveAsGTiff=False,
                               processes=50, skip=0)

if __name__ == '__main__':

    tic()
    doit()
    toc()

