from os.path import join
from lamos.datacube.types import MGRSFootprintCollection, WRS2FootprintCollection, LANDSAT_ANCHOR
from lamos.datacube.jobs import *
from hub.timing import tic, toc

def doit():

    rootDC = r'G:\_EnMAP\Rohdaten\Brazil\Raster\LANDSAT\datacubePESA'
    dir_aux = join(rootDC, 'auxiliary')
    shapefile = r'G:\_EnMAP\temp\temp_Marcel\PESA_samples\sample_px_centroids.shp'

    mgrsFootprints = MGRSFootprintCollection(['22LCH'])


    resolution = 30
    buffer = 300
    anchor = LANDSAT_ANCHOR

    job_import_shapefile_as_mask(shapefile=shapefile,
                                 outdirname=dir_aux,
                                 productname='samples', imagename='mask',
                                 mgrsFootprints=mgrsFootprints,
                                 resolution=resolution, buffer=buffer, anchor=anchor,
                                 processes=50, skip=0)

if __name__ == '__main__':

    tic()
    doit()
    toc()
