import numpy
import gdal
from os.path import join
import matplotlib.pyplot as plt

def getArray(filename):
    ds = gdal.Open(filename)
    return ds.ReadAsArray()

def getDates(filename):
    ds = gdal.Open(filename)
    tmp = ds.GetMetadataItem('wavelength', 'ENVI').strip()
    dates = map(float, tmp[1:-1].split(','))
    return dates

def script():

    root_dir = r'G:\_EnMAP\Rohdaten\Brazil\Raster\LANDSAT\datacubePESA\EVI_samples_out_20_1\22L\CH\timeseries'
    stepA = '_step_8'
    stepAB = '_step_8_8'
    stddevs = [8,16,32]

    # setup filenames
    forig = join(root_dir, 'evi_orig' + stepA + '_outFlt_20_1.tif')
    #forigII = join(root_dir, 'evi_orig' + stepA + '.tif')
    #forigIII = join(root_dir, 'evi_orig' + stepA + '_outFlt_20_1.tif')
    ffits = [join(root_dir, 'evi_stddev_' + str(stddev) + stepAB + '_outFlt_20_1.tif') for stddev in stddevs]
    fcounts = [join(root_dir, 'evi_counts_' + str(stddev) + stepAB +'_outFlt_20_1.tif') for stddev in stddevs]
    #forig = join(root_dir, 'tcg_orig' + stepA + '.tif')
    #ffits = [join(root_dir, 'tcg_stddev_' + str(stddev) + stepAB + '.tif') for stddev in stddevs]
    #fcounts = [join(root_dir, 'tcg_counts_' + str(stddev) + stepAB + '.tif') for stddev in stddevs]

    # read dates
    origDates = getDates(forig)
    #origDatesII = getDates(forigII)
    #origDatesIII = getDates(forigIII)
    ensembleDates = getDates(ffits[0])

    # read data
    orig = getArray(forig)
    #origII = getArray(forigII)
    #origIII = getArray(forigIII)
    fits = [getArray(ffit) for ffit in ffits]
    counts = [getArray(fcount) for fcount in fcounts]

    # calculate normalized weights
    countSum = numpy.sum(counts, axis=0)
    weights = [numpy.float32(count)/countSum for count in counts]

    # calculate ensemble
    ensemble = numpy.sum([fit*weight for fit, weight in zip(fits, weights)], axis=0)

    # plot original and fitted data for pixel (xi,yi)
    stdOne = fits[0]
    stdTwo = fits[1]
    stdThree = fits[2]

    for ipixel in range(12,19):
        yi, xi = ipixel, 0
        plt.plot(ensembleDates, ensemble[:,yi,xi],'-r')
        #plt.plot(ensembleDates, stdOne[:,yi,xi], '-g')
        #plt.plot(ensembleDates, stdTwo[:, yi, xi], '-b')
        #plt.plot(ensembleDates, stdThree[:, yi, xi], '-y')
        #plt.plot(origDatesIII, origIII[:, yi, xi], '.g')
        #plt.plot(origDatesII, origII[:, yi, xi], '.r')
        plt.plot(origDates, orig[:,yi,xi], '.')
        plt.show()


    # plot original and fitted data for pixel (xi,yi)

    #for ipixel in range(101,102):
    #    yi, xi = ipixel, 0
    #    plt.plot(ensembleDates, ensemble[:,yi,xi],'-r')
    #    #plt.plot(ensembleDates, fit[:,yi,xi], '-b')
    #    plt.plot(origDates, orig[:,yi,xi], '.')
    #    plt.show()

if __name__ == '__main__':
    script()