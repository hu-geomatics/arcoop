import os
import gdal
from os.path import join

VegIndex = 'EVI'
root_dir = r'G:\_EnMAP\Rohdaten\Brazil\Raster\LANDSAT\datacubePESA\productMGRS_PESA_out_20_1\22L\CH\timeseries'
#root_dir = r'G:\_EnMAP\temp\temp_Marcel\EVI_TS'

#stepA = '_step_8'
#stepAB = '_step_8_8'
#stddevs = [8, 16, 32]

#outname = join(root_dir, VegIndex + '_' +str(stddevs[0]) + '_' +str(stddevs[1])+ '_' +str(stddevs[2]) + '.bsq' )
outname = join(root_dir, 'EVI_8_16_32_ensemble_sub.bsq' )
bands = gdal.Open(outname).RasterCount

for band in range(1, bands+1, 1):
    outband = join(root_dir, 'bands' , VegIndex + '_ensemble_band_' + str(band).zfill(3) + '.bsq' )
    os.system("gdal_translate -of ENVI -b " + str(band) + " " + outname + " " +  outband)