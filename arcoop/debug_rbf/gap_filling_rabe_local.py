from os.path import join
from lamos.datacube.types import MGRSFootprintCollection
from lamos.datacube.applier import applierControls
from arcoop.debug_rbf.job_landsat_ndvi_timeseries_rbf_gap_filling import job_landsat_ndvi_timeseries_rbf_gap_filling
from hub.datetime import DateRangeCollection, DateRange, Date

def doit():

    dir_landsat = r'C:\Work\data\ms_pr\landsatSample'
    dir_product = r'C:\Work\data\ms_pr\productSample'

    mgrsFootprints = MGRSFootprintCollection(['37SEB'])
    dateRange = DateRange(start=Date(2015, 1, 1), end=Date(2015, 12, 31))
    stepFit = 1 # fit on 5-days bins
    stepPredict = 10 # predict on (5*2=)10-days bins

    stddevs = [3,5,7]
    wsize = 64
    controls = applierControls(blockxsize=wsize, blockysize=wsize, wsize=wsize)
    job_landsat_ndvi_timeseries_rbf_gap_filling(indirname=dir_landsat, outdirname=dir_product,
                                                stddevs=stddevs,
                                                mgrsFootprints=mgrsFootprints,
                                                productname='timeseries', imagename='ndvi',
                                                dateRange=dateRange,
                                                stepFit=stepFit, stepPredict=stepPredict,
                                                controls=controls,
                                                processes=1, skip=False)

if __name__ == '__main__':
    from osgeo import gdal
    gdal.SetCacheMax(1)
    doit()
