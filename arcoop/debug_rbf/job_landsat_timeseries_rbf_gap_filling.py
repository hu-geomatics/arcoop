from __future__ import division, print_function
import numpy
from os.path import join
from lamos.datacube.types import MGRSFootprintCollection, yieldSpectralTilesOfMGRSFootprint
from lamos.datacube.applier import applierControls, applierSetup, ufuncWrapper
from rios.applier import apply
from astropy.convolution import Gaussian1DKernel, convolve
from hub.datetime import DateRangeCollection, DateRange, Date
from datetime import timedelta
from hub.gdal.api import GDALMeta


def ufunc(info, inputs, outputs, inmetas, otherArgs, stddevs):
    _, lines, samples = inputs.cfmask[0].shape

    # init vegetation index array with 1 day resolution
    dateRange = otherArgs.dateRange
    stepFit = otherArgs.stepFit
    stepPredict = otherArgs.stepPredict

    bands = (dateRange.end - dateRange.start).days // stepFit + 1
    vegIndex = otherArgs.vegIndex

    # read input images and calculate selected vegetation index
    vegIndex_ts = numpy.full((bands, lines, samples), fill_value=numpy.nan, dtype=numpy.float32)
    for isrmeta, icfmask, isr in zip(inmetas.sr, inputs.cfmask, inputs.sr):
        iblue = isr[isrmeta.getBandNames().index('blue')]
        igreen = isr[isrmeta.getBandNames().index('green')]
        ired = isr[isrmeta.getBandNames().index('red')]
        inir = isr[isrmeta.getBandNames().index('nir')]
        iswir1 = isr[isrmeta.getBandNames().index('swir1')]
        iswir2 = isr[isrmeta.getBandNames().index('swir2')]
        if vegIndex == 'tcg':
            iTCG = numpy.float32((iblue * -0.1603) + (igreen * -0.2819) + (ired * -0.4934) + (inir * 0.7940) + (iswir1 * -0.0002) + (iswir2 * -0.1446))
            iTCG[icfmask[0] != 0] = numpy.nan
            index = (isrmeta.getAcquisitionDate() - dateRange.start).days
            index = index // stepFit
            vegIndex_ts[index] = iTCG
        elif vegIndex == 'tcb':
            iTCB = numpy.float32(iblue * 0.2043 + igreen * 0.4158 + ired * 0.5524 + inir * 0.5741 + iswir1 * 0.3124 + iswir2 * 0.2303)
            iTCB[icfmask[0] != 0] = numpy.nan
            index = (isrmeta.getAcquisitionDate() - dateRange.start).days
            index = index // stepFit
            vegIndex_ts[index] = iTCB
        elif vegIndex == 'tcw':
            iTCW = numpy.float32(iblue * 0.0315 + igreen * 0.2021 + ired * 0.3102 + inir * 0.1594 + iswir1 * -0.6806 + iswir2 * -0.6109)
            iTCW[icfmask[0] != 0] = numpy.nan
            index = (isrmeta.getAcquisitionDate() - dateRange.start).days
            index = index // stepFit
            vegIndex_ts[index] = iTCW
        elif vegIndex == 'ndvi':
            indvi = numpy.float32(inir - ired) / numpy.float32(inir + ired) * 10000.
            indvi[icfmask[0] != 0] = numpy.nan
            index = (isrmeta.getAcquisitionDate() - dateRange.start).days
            index = index // stepFit
            vegIndex_ts[index] = indvi
        elif vegIndex == 'evi':
            ievi = numpy.float32(2.5 * (numpy.float32(inir - ired) / numpy.float32(inir + (6 * ired) - (7.5 * iblue) + 10000))) * 10000.
            ievi[icfmask[0] != 0] = numpy.nan
            index = (isrmeta.getAcquisitionDate() - dateRange.start).days
            index = index // stepFit
            vegIndex_ts[index] = ievi
        elif vegIndex == 'ndmi':
            indmi = numpy.float32(inir - iswir1) / numpy.float32(inir + iswir1) * 10000.
            indmi[icfmask[0] != 0] = numpy.nan
            index = (isrmeta.getAcquisitionDate() - dateRange.start).days
            index = index // stepFit
            vegIndex_ts[index] = indmi


    # if argument outlier filter is true start outlier detection based on a selected RBF filter
    # and a pixelwise standard deviations times the defined multiplier

    vegIndex_ts_flt = vegIndex_ts

    if otherArgs.outlierFlt == True:
        outlierStd = otherArgs.outlierStd
        outlierStd = outlierStd / stepFit
        kernel = Gaussian1DKernel(outlierStd).array.reshape((-1, 1, 1))
        outlier_filled = convolve(vegIndex_ts, kernel, boundary='fill', fill_value=numpy.nan, normalize_kernel=True)
        outlier_filled_float = outlier_filled.astype(numpy.float32)
        out_std_px = numpy.nanstd(outlier_filled_float, axis=0)
        outlier_filled[numpy.logical_not(numpy.isfinite(outlier_filled))] = numpy.iinfo(numpy.int16).min
        outlier_filled = outlier_filled.astype(numpy.int16)


        stdTimes = otherArgs.stdTimes
        out_std_px_times = out_std_px * stdTimes

        for i in range(bands):
            ivegIndex_ts = vegIndex_ts[i]
            upperLimit = (outlier_filled[i] + out_std_px_times)
            lowerLimit = (outlier_filled[i] - out_std_px_times)
            vegIndex_ts_flt[i] = numpy.where((ivegIndex_ts > upperLimit), numpy.nan, vegIndex_ts_flt[i])
            vegIndex_ts_flt[i] = numpy.where((ivegIndex_ts < lowerLimit), numpy.nan, vegIndex_ts_flt[i])

    outputs.ts_orig = vegIndex_ts_flt.astype(numpy.int16)
    clear_ts = numpy.isfinite(vegIndex_ts_flt).astype(numpy.float32)  # need to convert to float32, because convolve will produce a float64 otherwise
    outputs.ts_counts = list()

    # start timeseries gap filling for each of the selected kernel width
    outputs.ts_filled = list()
    for stddev in stddevs:
        stddev = stddev / stepFit
        kernel = Gaussian1DKernel(stddev).array.reshape((-1, 1, 1))
        ts_filled = convolve(vegIndex_ts_flt, kernel, boundary='fill', fill_value=numpy.nan, normalize_kernel=True)
        ts_filled[numpy.logical_not(numpy.isfinite(ts_filled))] = numpy.iinfo(numpy.int16).min
        ts_filled = ts_filled.astype(numpy.int16)
        ts_counts = convolve(clear_ts, kernel, boundary='fill', fill_value=0, normalize_kernel=True)
        ts_counts *= 10000
        ts_counts = ts_counts.astype(numpy.int16)

        if stepPredict > 1:
            ts_filled = ts_filled[::stepPredict]
            ts_counts = ts_counts[::stepPredict]

        outputs.ts_filled.append(ts_filled)
        outputs.ts_counts.append(ts_counts)


def job_landsat_timeseries_rbf_gap_filling(indirname, outdirname, mgrsFootprints,
                                              stddevs,
                                              dateRange,
                                              stepFit, stepPredict,
                                              vegIndex,
                                              outlierFlt, outlierStd, stdTimes,
                                              productname='timeseries', imagename='tc',
                                              controls=applierControls(),
                                              processes=1, skip=False):
    if skip: return

    assert isinstance(mgrsFootprints, MGRSFootprintCollection)
    assert isinstance(dateRange, DateRange)

    controls.setNumThreads(processes)
    if vegIndex == 'tcg':
        print('Landsat Tasseled Cap Greenness Timeseries Gap Filling')
    elif vegIndex == 'tcb':
        print('Landsat Tasseled Cap Brightness Timeseries Gap Filling')
    elif vegIndex == 'tcw':
        print('Landsat Tasseled Cap Wetness Timeseries Gap Filling')
    elif vegIndex == 'ndvi':
        print('Landsat NDVI Timeseries Gap Filling')
    elif vegIndex == 'ndmi':
        print('Landsat NDMI Timeseries Gap Filling')
    elif vegIndex == 'evi':
        print('Landsat EVI Timeseries Gap Filling')
    for footprint in mgrsFootprints:
        print(footprint.name)

        infiles, outfiles, otherArgs = applierSetup(ufunc=ufunc, stddevs=stddevs)
        infiles.cfmask = list()
        infiles.sr = list()
        for scene in yieldSpectralTilesOfMGRSFootprint(root=indirname, footprint=footprint, dateRange=dateRange):
            infiles.cfmask.append(scene.qa_filename)
            infiles.sr.append(scene.sr_filename)

        if len(infiles.cfmask) == 0:
            print('Warning: no observations available!')
            return

        otherArgs.inmetas.readMeta(infiles)
        otherArgs.dateRange = dateRange
        otherArgs.stepFit = stepFit
        otherArgs.stepPredict = stepPredict
        otherArgs.outlierFlt = outlierFlt
        otherArgs.outlierStd = outlierStd
        otherArgs.stdTimes = stdTimes
        otherArgs.vegIndex = vegIndex

        controls.selectInputImageLayers(layerselection=[1], imagename='cfmask')  # cfmask is first band in qa stack

        outfiles.ts_filled = list()
        outfiles.ts_counts = list()

        if otherArgs.outlierFlt == True:
            outfiles.ts_orig = join(outdirname, footprint.subfolders[0], footprint.subfolders[1], productname,
                                    imagename + '_orig_step_' + str(stepFit) + '_outFlt_' + str(outlierStd) + '_' + str(
                                    stdTimes) + '.tif')
            for stddev in stddevs:
                dirname = join(outdirname, footprint.subfolders[0], footprint.subfolders[1], productname)
                outfiles.ts_filled.append(join(dirname, imagename + '_stddev_' + str(stddev) + '_step_' + str(
                    stepFit) + '_' + str(stepFit * stepPredict) + '_outFlt_' + str(outlierStd) + '_' + str(
                    stdTimes) + '.tif'))
                outfiles.ts_counts.append(join(dirname, imagename + '_counts_' + str(stddev) + '_step_' + str(
                    stepFit) + '_' + str(stepFit * stepPredict) + '_outFlt_' + str(outlierStd) + '_' + str(
                    stdTimes) + '.tif'))
        else:
            outfiles.ts_orig = join(outdirname, footprint.subfolders[0], footprint.subfolders[1], productname,
                                    imagename + '_orig_step_' + str(stepFit) + '.tif')
            for stddev in stddevs:
                dirname = join(outdirname, footprint.subfolders[0], footprint.subfolders[1], productname)
                outfiles.ts_filled.append(join(dirname, imagename + '_stddev_' + str(stddev) + '_step_' + str(
                    stepFit) + '_' + str(stepFit * stepPredict) + '.tif'))
                outfiles.ts_counts.append(join(dirname, imagename + '_counts_' + str(stddev) + '_step_' + str(
                    stepFit) + '_' + str(stepFit * stepPredict) + '.tif'))

        # apply the user function
        apply(userFunction=ufuncWrapper, infiles=infiles, outfiles=outfiles,
              otherArgs=otherArgs, controls=controls)

        # set metadata
        bandNames = list()
        wavelength = list()
        for days in range((otherArgs.dateRange.end - otherArgs.dateRange.start).days + 1):
            date = Date.fromText(str(dateRange.start + timedelta(days=days)))
            bandNames.append(str(date))
            wavelength.append(date.decimalYear)

        outfilesAndSteps = list()
        outfilesAndSteps.append((outfiles.ts_orig, stepFit))
        for f in outfiles.ts_filled + outfiles.ts_counts:
            outfilesAndSteps.append((f, stepFit * stepPredict))

        for outfile, step in outfilesAndSteps:
            outmeta = GDALMeta(outfile)
            outmeta.setBandNames(bandNames[::step])
            outmeta.setNoDataValue(numpy.iinfo(numpy.int16).min)
            outmeta.setMetadataItem('wavelength', wavelength[::step])
            outmeta.writeMeta()
