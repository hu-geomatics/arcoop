from __future__ import division, print_function
import numpy
from os.path import join
from lamos.datacube.types import MGRSFootprintCollection, yieldSpectralTilesOfMGRSFootprint
from lamos.datacube.applier import applierControls, applierSetup, ufuncWrapper
from rios.applier import apply
from astropy.convolution import Gaussian1DKernel, convolve
from hub.datetime import DateRangeCollection, DateRange, Date
from datetime import timedelta
from hub.gdal.api import GDALMeta

def ufunc(info, inputs, outputs, inmetas, otherArgs, stddevs):

    _, lines, samples = inputs.cfmask[0].shape

    # init ndvi array with 1 day resolution
    dateRange = otherArgs.dateRange
    stepFit = otherArgs.stepFit
    stepPredict = otherArgs.stepPredict

    #bands = (dateRange.end - dateRange.start).days + 1
    #bands = bands // stepFit
    bands = (dateRange.end - dateRange.start).days // stepFit + 1

    ndvi_ts = numpy.full((bands, lines, samples), fill_value=numpy.nan, dtype=numpy.float32)
    for isrmeta, icfmask, isr in zip(inmetas.sr, inputs.cfmask, inputs.sr):
        inir = isr[isrmeta.getBandNames().index('nir')]
        ired = isr[isrmeta.getBandNames().index('red')]
        indvi = numpy.float32(inir - ired) / numpy.float32(inir + ired) * 10000.
        indvi[icfmask[0] != 0] = numpy.nan
        index = (isrmeta.getAcquisitionDate() - dateRange.start).days
        index = index // stepFit
        ndvi_ts[index] = indvi

    outputs.ts_orig = ndvi_ts.astype(numpy.int16)

    clear_ts = numpy.isfinite(ndvi_ts).astype(numpy.float32)  # need to convert to float32, because convolve will produce a float64 otherwise
    outputs.ts_counts = list()

    outputs.ts_filled = list()
    for stddev in stddevs:
        stddev = stddev / stepFit
        kernel = Gaussian1DKernel(stddev).array.reshape((-1,1,1))
        ts_filled = convolve(ndvi_ts, kernel, boundary='fill', fill_value=numpy.nan, normalize_kernel=True)
        ts_filled[numpy.logical_not(numpy.isfinite(ts_filled))] = numpy.iinfo(numpy.int16).min
        ts_filled = ts_filled.astype(numpy.int16)
        ts_counts = convolve(clear_ts, kernel, boundary='fill', fill_value=0, normalize_kernel=True)
        ts_counts *= 10000
        ts_counts = ts_counts.astype(numpy.int16)

        if stepPredict > 1:
            ts_filled = ts_filled[::stepPredict]
            ts_counts = ts_counts[::stepPredict]

        outputs.ts_filled.append(ts_filled)
        outputs.ts_counts.append(ts_counts)


def job_landsat_ndvi_timeseries_rbf_gap_filling(indirname, outdirname, mgrsFootprints,
                                                stddevs,
                                                dateRange,
                                                stepFit, stepPredict,
                                                productname='timeseries', imagename='ndvi',
                                                controls=applierControls(),
                                                processes=1, skip=False):

    if skip: return

    assert isinstance(mgrsFootprints, MGRSFootprintCollection)
    assert isinstance(dateRange, DateRange)

    controls.setNumThreads(processes)
    print('Landsat NDVI Timeseries Gap Filling')
    for footprint in mgrsFootprints:
        print(footprint.name)

        infiles, outfiles, otherArgs = applierSetup(ufunc=ufunc, stddevs=stddevs)
        infiles.cfmask = list()
        infiles.sr = list()
        for scene in yieldSpectralTilesOfMGRSFootprint(root=indirname, footprint=footprint, dateRange=dateRange):
            infiles.cfmask.append(scene.qa_filename)
            infiles.sr.append(scene.sr_filename)

        if len(infiles.cfmask)==0:
            print('Warning: no observations available!')
            return

        otherArgs.inmetas.readMeta(infiles)
        otherArgs.dateRange = dateRange
        otherArgs.stepFit = stepFit
        otherArgs.stepPredict = stepPredict

        controls.selectInputImageLayers(layerselection=[1], imagename='cfmask') # cfmask is first band in qa stack

        outfiles.ts_orig = join(outdirname, footprint.subfolders[0], footprint.subfolders[1], productname,
                                imagename + '_orig_step_'+str(stepFit) + '.tif')
        outfiles.ts_filled = list()
        outfiles.ts_counts = list()

        for stddev in stddevs:
            dirname = join(outdirname, footprint.subfolders[0], footprint.subfolders[1], productname)
            outfiles.ts_filled.append(join(dirname, imagename + '_stddev_'+str(stddev) + '_step_'+str(stepFit)+'_'+str(stepFit*stepPredict) + '.tif'))
            outfiles.ts_counts.append(join(dirname, imagename + '_counts_' + str(stddev) + '_step_'+str(stepFit)+'_'+str(stepFit*stepPredict) + '.tif'))

        # apply the user function
        apply(userFunction=ufuncWrapper, infiles=infiles, outfiles=outfiles,
                           otherArgs=otherArgs, controls=controls)

        # set metadata
        bandNames = list()
        wavelength = list()
        for days in range((otherArgs.dateRange.end - otherArgs.dateRange.start).days+1):
            #date = Date.fromDate(otherArgs.dateRange.start+timedelta(days=days))
            date = Date.fromText(str(dateRange.start + timedelta(days=days)))
            bandNames.append(str(date))
            wavelength.append(date.decimalYear)

        outfilesAndSteps = list()
        outfilesAndSteps.append((outfiles.ts_orig, stepFit))
        for f in outfiles.ts_filled+outfiles.ts_counts:
            outfilesAndSteps.append((f, stepFit * stepPredict))

        for outfile, step in outfilesAndSteps:
            outmeta = GDALMeta(outfile)
            outmeta.setBandNames(bandNames[::step])
            outmeta.setMetadataItem('wavelength', wavelength[::step])
            outmeta.writeMeta()
