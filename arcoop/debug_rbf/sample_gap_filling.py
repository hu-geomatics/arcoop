from os.path import join
from lamos.datacube.types import MGRSFootprintCollection
from lamos.datacube.jobs import *
from hub.timing import tic, toc
from arcoop.debug_rbf.job_landsat_timeseries_rbf_gap_filling import *



def doit():

    rootDC = r'G:\_EnMAP\Rohdaten\Brazil\Raster\LANDSAT\datacubePETR'
    dir_landsat = join(rootDC, 'landsatSample')
    dir_product = join(rootDC, 'RBF_TC_1-50_samples_outFlt_test_index')

    mgrsFootprints = MGRSFootprintCollection(['23LLE'])

    dateRange = DateRange(start=Date(2014, 1, 1), end=Date(2015, 12, 31))
    stepFit = 1 # fit on 5-days bins
    stepPredict = 10 # predict on (5*2=)10-days bins

    stddevs = range(1, 10)
    wsize = 64
    controls = applierControls(blockxsize=wsize, blockysize=wsize, wsize=wsize)
    job_landsat_timeseries_rbf_gap_filling(indirname=dir_landsat, outdirname=dir_product,
                                                stddevs=stddevs,
                                                outlierFlt=True,
                                                outlierStd=20,
                                                stdTimes=1,
                                                vegIndex='tcb',
                                                mgrsFootprints=mgrsFootprints,
                                                productname='timeseries', imagename='tcb',
                                                dateRange=dateRange,
                                                stepFit=stepFit, stepPredict=stepPredict,
                                                controls=controls,
                                                processes=1, skip=False)
if __name__ == '__main__':
    tic()
    doit()
    toc()
