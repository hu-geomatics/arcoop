import sys
import json
import logging
from collections import deque
from gms.io.GMSCubeInputFormat import CubeInputFormat as CubeIF
from gms.io.GMSCubeOutputFormat import CubeOutputFormat as CubeOF
from gms.operators.Compositor import Compositor
from gms.jobs.SplitCollector import Collector
from gms.operators.CubeMerger import CubeMerger
from gms.operators.Warper import Warper

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=1)

def main():

    debug = 3
    print("Driver program for HUB-GEO classification use case")

    config_path = r'C:\Work\source\arcoop\gms_test\config.json'

    with open(config_path) as json_file:
        config = json.load(json_file)

    output_path = config["output"]["path"]
    compositor_config = config['compositor']
    wave_keys = compositor_config["wavekeys"]
    align = bool(config["input"]["align"])
    input_config = config["input"]

    if align:
        wavelengths = [float(numeric_string) for numeric_string in wave_keys[0:len(wave_keys) - 1]]
    else:
        wavelengths = None

    inputFormat = CubeIF(config=input_config, wavelengths=wavelengths, debug=debug)
    collector = Collector()

    # read the data
    inputFormat.createInputSplits(0, "", collector)
    # enable pipelined execution and result compression
    composites = Collector()
    compositor = Compositor(config=compositor_config)
    compositor._collector = composites

    for s in collector.retrieveElems():
        #dirty hack: FileInputSplits are treated as tuples once they are transferred from the JobManager
        split = (s.path, s.start, (s.end - s.start), s.additional)
        inputFormat.deliver(split, compositor)


    groups = dict()
    for c in composites.retrieveElems():
        key = c.mgrs_tile_id
        if key not in groups:
            groups[key] = deque()
        groups[key].append(c)

    warper = Warper()
    merged = Collector()
    warper._collector = merged
    merger = CubeMerger(deserialize=False, serialize=False)
    for g in groups:
        sc_list = groups[g]
        merger.reduce(sc_list, warper)

    output = CubeOF(base_path=output_path, overwrite=True, deserialize=False)
    for classif in merged.retrieveElems():
        output.write(classif)


if __name__ == "__main__":
    main()
