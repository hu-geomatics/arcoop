from os.path import join

from debug_rbf.job_landsat_tc_timeseries_rbf_gap_filling import job_landsat_tc_timeseries_rbf_gap_filling

from hub.datetime import DateRange, Date
from lamos.datacube.applier import applierControls
from lamos.datacube.types import MGRSFootprintCollection


def doit():


    rootDC = r'G:\_EnMAP\Rohdaten\Brazil\Raster\LANDSAT\datacubePNCV'
    dir_landsat = join(rootDC, 'landsat')
    dir_product = join(rootDC, 'productMGRS_test')

    mgrsFootprints = MGRSFootprintCollection(['23LLE'])
    # mgrsFootprints = MGRSFootprintCollection(['23LKD'])  # , '22LHH', '23LJC', '23LKC', '23LLC', '23LMC', '22KHH', '23KJC', '23KKC', '23KKB', '23KMB'])
    dateRange = DateRange(start=Date(2014, 1, 1), end=Date(2016, 12, 31))
    stepFit = 5  # fit on 5-days bins
    stepPredict = 2  # predict on (5*2=)10-days bins

    stddevs = [21]
    wsize = 64
    controls = applierControls(blockxsize=wsize, blockysize=wsize, wsize=wsize)
    job_landsat_tc_timeseries_rbf_gap_filling(indirname=dir_landsat, outdirname=dir_product,
                                                stddevs=stddevs,
                                                mgrsFootprints=mgrsFootprints,
                                                productname='timeseries', imagename='ndvi',
                                                dateRange=dateRange,
                                                stepFit=stepFit, stepPredict=stepPredict,
                                                controls=controls,
                                                processes=1, skip=False)

if __name__ == '__main__':
    from osgeo import gdal
    gdal.SetCacheMax(1)
    doit()
