from __future__ import division, print_function
import numpy
from os.path import join
from lamos.datacube.types import MGRSFootprintCollection, yieldSpectralTilesOfMGRSFootprint
from lamos.datacube.applier import applierControls, applierSetup, ufuncWrapper
from rios.applier import apply
from astropy.convolution import Gaussian1DKernel, convolve
from hub.datetime import DateRangeCollection, DateRange, Date
from datetime import timedelta
from hub.gdal.api import GDALMeta

def ufunc(info, inputs, outputs, inmetas, otherArgs, stddevs):

    _, lines, samples = inputs.cfmask[0].shape

    # init ndvi array with 1 day resolution
    dateRange = otherArgs.dateRange
    stepFit = otherArgs.stepFit
    stepPredict = otherArgs.stepPredict

    #bands = (dateRange.end - dateRange.start).days + 1
    #bands = bands // stepFit
    bands = (dateRange.end - dateRange.start).days // stepFit + 1

    tcg_ts = numpy.full((bands, lines, samples), fill_value=numpy.nan, dtype=numpy.float32)
    for isrmeta, icfmask, isr in zip(inmetas.sr, inputs.cfmask, inputs.sr):
        iblue = isr[isrmeta.getBandNames().index('blue')]
        igreen = isr[isrmeta.getBandNames().index('green')]
        ired = isr[isrmeta.getBandNames().index('red')]
        inir = isr[isrmeta.getBandNames().index('nir')]
        iswir1 = isr[isrmeta.getBandNames().index('swir1')]
        iswir2 = isr[isrmeta.getBandNames().index('swir2')]
        iTCG = numpy.float32((iblue * -0.1603) + (igreen * -0.2819) + (ired + -0.4934) + (inir * 0.7940) + (iswir1 * -0.0002) + (iswir2 * -0.1446))
        #iTCB = numpy.int32(iblue * 0.2043 + igreen * 0.4158 + ired * 0.5524 + inir * 0.5741 + iswir1 * 0.3124 + iswir2 * 0.2303)
        #iTCW = numpy.int32(iblue * 0.0315 + igreen * 0.2021 + ired * 0.3102 + inir * 0.1594 + iswir1 * -0.6806 + iswir2 * -0.6109)
        # inir = isr[isrmeta.getBandNames().index('nir')]
        # ired = isr[isrmeta.getBandNames().index('red')]
        # indvi = numpy.float32(inir - ired) / numpy.float32(inir + ired) * 10000.
        iTCG[icfmask[0] != 0] = numpy.nan
        index = (isrmeta.getAcquisitionDate() - dateRange.start).days
        index = index // stepFit
        tcg_ts[index] = iTCG

    outputs.ts_orig = tcg_ts.astype(numpy.int16)

    clear_ts = numpy.isfinite(tcg_ts).astype(numpy.float32)  # need to convert to float32, because convolve will produce a float64 otherwise
    outputs.ts_counts = list()

    outputs.ts_filled = list()
    for stddev in stddevs:
        stddev = stddev / stepFit
        kernel = Gaussian1DKernel(stddev).array.reshape((-1,1,1))
        ts_filled = convolve(tcg_ts, kernel, boundary='fill', fill_value=numpy.nan, normalize_kernel=True)
        ts_filled[numpy.logical_not(numpy.isfinite(ts_filled))] = numpy.iinfo(numpy.int16).min
        ts_filled = ts_filled.astype(numpy.int16)
        ts_counts = convolve(clear_ts, kernel, boundary='fill', fill_value=0, normalize_kernel=True)
        ts_counts *= 10000
        ts_counts = ts_counts.astype(numpy.int16)

        if stepPredict > 1:
            ts_filled = ts_filled[::stepPredict]
            ts_counts = ts_counts[::stepPredict]

        outputs.ts_filled.append(ts_filled)
        outputs.ts_counts.append(ts_counts)


def job_landsat_tc_timeseries_rbf_gap_filling(indirname, outdirname, mgrsFootprints,
                                                stddevs,
                                                dateRange,
                                                stepFit, stepPredict,
                                                productname='timeseries', imagename='tcg',
                                                controls=applierControls(),
                                                processes=1, skip=False):

    if skip: return

    assert isinstance(mgrsFootprints, MGRSFootprintCollection)
    assert isinstance(dateRange, DateRange)

    controls.setNumThreads(processes)
    print('Landsat Tasseled Cap Timeseries Gap Filling')
    for footprint in mgrsFootprints:
        print(footprint.name)

        infiles, outfiles, otherArgs = applierSetup(ufunc=ufunc, stddevs=stddevs)
        infiles.cfmask = list()
        infiles.sr = list()
        for scene in yieldSpectralTilesOfMGRSFootprint(root=indirname, footprint=footprint, dateRange=dateRange):
            infiles.cfmask.append(scene.qa_filename)
            infiles.sr.append(scene.sr_filename)

        if len(infiles.cfmask)==0:
            print('Warning: no observations available!')
            return

        otherArgs.inmetas.readMeta(infiles)
        otherArgs.dateRange = dateRange
        otherArgs.stepFit = stepFit
        otherArgs.stepPredict = stepPredict

        controls.selectInputImageLayers(layerselection=[1], imagename='cfmask') # cfmask is first band in qa stack

        outfiles.ts_orig = join(outdirname, footprint.subfolders[0], footprint.subfolders[1], productname,
                                imagename + '_orig_step_'+str(stepFit) + '.tif')
        outfiles.ts_filled = list()
        outfiles.ts_counts = list()

        for stddev in stddevs:
            dirname = join(outdirname, footprint.subfolders[0], footprint.subfolders[1], productname)
            outfiles.ts_filled.append(join(dirname, imagename + '_stddev_'+str(stddev) + '_step_'+str(stepFit)+'_'+str(stepFit*stepPredict) + '.tif'))
            outfiles.ts_counts.append(join(dirname, imagename + '_counts_' + str(stddev) + '_step_'+str(stepFit)+'_'+str(stepFit*stepPredict) + '.tif'))

        # apply the user function
        apply(userFunction=ufuncWrapper, infiles=infiles, outfiles=outfiles,
                           otherArgs=otherArgs, controls=controls)

        # set metadata
        bandNames = list()
        wavelength = list()
        for days in range((otherArgs.dateRange.end - otherArgs.dateRange.start).days+1):
            #date = Date.fromDate(otherArgs.dateRange.start+timedelta(days=days))
            date = Date.fromText(str(dateRange.start + timedelta(days=days)))
            bandNames.append(str(date))
            wavelength.append(date.decimalYear)

        for outfile, step in zip(outfiles.ts_filled + outfiles.ts_counts + [outfiles.ts_orig],
                                 [stepFit*stepPredict, stepFit*stepPredict, stepFit]):
            outmeta = GDALMeta(outfile)
            outmeta.setBandNames(bandNames[::step])
            outmeta.setMetadataItem('wavelength', wavelength[::step])
            outmeta.writeMeta()
