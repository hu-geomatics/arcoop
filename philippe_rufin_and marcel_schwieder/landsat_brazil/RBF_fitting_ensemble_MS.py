import numpy
import gdal
from os.path import join
import matplotlib.pyplot as plt

def getArray(filename):
    ds = gdal.Open(filename)
    return ds.ReadAsArray()

def getDates(filename):
    ds = gdal.Open(filename)
    tmp = ds.GetMetadataItem('wavelength', 'ENVI').strip()
    dates = map(float, tmp[1:-1].split(','))
    return dates

def script():

    #root_dir = r'C:\Work\data\ms_pr\productSample\37S\EB\timeseries'
    root_dir = r'G:\_EnMAP\Rohdaten\Brazil\Raster\LANDSAT\datacubePETR\RBF_TC_1-50_samples\23L\LE\timeseries'
    stepA = '_step_1'
    stepAB = '_step_1_10'
    stddevs = [30, 30, 30]

    # setup filenames
    forig = join(root_dir, 'tc_orig' + stepA + '.tif')
    ffits = [join(root_dir, 'tc_stddev_' + str(stddev) + stepAB + '.tif') for stddev in stddevs]
    fcounts = [join(root_dir, 'tc_counts_' + str(stddev) + stepAB + '.tif') for stddev in stddevs]

    # read dates
    origDates = getDates(forig)
    ensembleDates = getDates(ffits[0])

    # read data
    orig = getArray(forig)
    fits = [getArray(ffit) for ffit in ffits]
    counts = [getArray(fcount) for fcount in fcounts]

    # calculate normalized weights
    countSum = numpy.sum(counts, axis=0)
    weights = [numpy.float32(count)/countSum for count in counts]

    # calculate ensemble
    ensemble = numpy.sum([fit*weight for fit, weight in zip(fits, weights)], axis=0)

    # plot original and fitted data for pixel (xi,yi)
    stdOne = fits[0]
    stdTwo = fits[1]
    stdThree = fits[2]

    out_std_px = [numpy.nanstd(stdOne[:, px, 0], axis=0) for px in range(1,10)]

    for ipixel in range(1,2):
        yi, xi = ipixel, 0
        plt.plot(ensembleDates, ensemble[:,yi,xi],'-r')
        plt.plot(ensembleDates, stdOne[:,yi,xi], '-b')
        plt.plot(ensembleDates, stdTwo[:, yi, xi], '-b')
        plt.plot(ensembleDates, stdThree[:, yi, xi], '-b')
        plt.plot(origDates, orig[:,yi,xi], '.')
        plt.show()


    # plot original and fitted data for pixel (xi,yi)

    #for ipixel in range(101,102):
    #    yi, xi = ipixel, 0
    #    plt.plot(ensembleDates, ensemble[:,yi,xi],'-r')
    #    #plt.plot(ensembleDates, fit[:,yi,xi], '-b')
    #    plt.plot(origDates, orig[:,yi,xi], '.')
    #    plt.show()

if __name__ == '__main__':
    script()