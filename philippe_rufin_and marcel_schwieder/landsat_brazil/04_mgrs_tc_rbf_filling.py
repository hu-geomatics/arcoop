from os.path import join
from hub.datetime import DateRange, Date
from lamos.datacube.applier import applierControls
from lamos.datacube.types import MGRSFootprintCollection
from job_landsat_tc_timeseries_rbf_gap_filling import job_landsat_tc_timeseries_rbf_gap_filling


def doit():

    rootDC = r'G:\_EnMAP\Rohdaten\Brazil\Raster\LANDSAT\datacubePNCV'

    dir_landsat = join(rootDC, 'landsat')
    dir_product = join(rootDC, 'productMGRS_PNCV')

    mgrsFootprints = MGRSFootprintCollection(['23LKE'])



    dateRange = DateRange(start=Date(2014, 1, 1), end=Date(2015, 12, 31))
    stepFit = 5  # fit on 5-days bins
    stepPredict = 2  # predict on (5*2=)10-days bins

    stddevs = [15, 25, 35]
    wsize = 64
    controls = applierControls(blockxsize=wsize, blockysize=wsize, wsize=wsize)
    job_landsat_tc_timeseries_rbf_gap_filling(indirname=dir_landsat, outdirname=dir_product,
                                              stddevs=stddevs,
                                              mgrsFootprints=mgrsFootprints,
                                              productname='timeseries', imagename='tcg',
                                              dateRange=dateRange,
                                              stepFit=stepFit, stepPredict=stepPredict,
                                              controls=controls,
                                              processes=70, skip=False)

if __name__ == '__main__':
    from osgeo import gdal
    gdal.SetCacheMax(1)
    doit()