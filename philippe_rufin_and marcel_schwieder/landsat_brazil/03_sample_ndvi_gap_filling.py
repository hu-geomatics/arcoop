from os.path import join
from lamos.datacube.types import MGRSFootprintCollection
from lamos.datacube.jobs import *
from hub.timing import tic, toc

def doit():

    rootDC = r'G:\_EnMAP\Rohdaten\Brazil\Raster\LANDSAT\datacube'
    dir_landsatSample = join(rootDC, 'landsatSample')
    dir_productSample = join(rootDC, 'productSample3')

    mgrsFootprints = MGRSFootprintCollection(['23LKD', '22LHH', '23LJC', '23LKC', '23LLC', '23LMC', '22KHH', '23KJC', '23KKC', '23KKB', '23KMB'])

    stddevs = range(1, 51)
    job_landsat_ndvi_timeseries_rbf_gap_filling(indirname=dir_landsatSample, outdirname=dir_productSample,
                                                stddevs=stddevs,
                                                mgrsFootprints=mgrsFootprints,
                                                productname='timeseries', imagename='ndvi',
                                                processes=1, skip=False)

if __name__ == '__main__':
    tic()
    doit()
    toc()
