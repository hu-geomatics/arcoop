from os.path import join
from lamos.datacube.types import MGRSFootprintCollection
from lamos.datacube.jobs import *
from hub.timing import tic, toc

def doit():

    rootDC = r'\\141.20.140.91\SAN_RSDBrazil\LandsatData\Landsat_Turkey\02_OutputData\LandsatX\datacube'
    dir_landsat = join(rootDC, 'landsat')
    dir_aux = join(rootDC, 'auxiliary')
    dir_landsatSample = join(rootDC, 'landsatSample')

    mgrsFootprints = MGRSFootprintCollection(['37SEB'])

    job_sample(indirname=dir_landsat, outdirname=dir_landsatSample,
               locdirname=dir_aux, locproductname='n25', locimagename='mask',
               mgrsFootprints=mgrsFootprints,
               processes=50, skip=0)

if __name__ == '__main__':

    tic()
    doit()
    toc()