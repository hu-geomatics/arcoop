from os.path import join
from lamos.datacube.types import MGRSFootprintCollection
from lamos.datacube.applier import applierControls
from arcoop.debug_rbf.job_landsat_ndvi_timeseries_rbf_gap_filling import job_landsat_ndvi_timeseries_rbf_gap_filling
from hub.datetime import DateRangeCollection, DateRange, Date

def doit():

    dir_landsat = r'M:\LandsatData\Landsat_Turkey\02_OutputData\LandsatX\datacube\landsat'
    dir_product = r'M:\LandsatData\Landsat_Turkey\02_OutputData\LandsatX\datacube\products'

    mgrsFootprints = MGRSFootprintCollection(['35SNC'])
    dateRange = DateRange(start=Date(2014, 1, 1), end=Date(2016, 12, 31))
    stepFit = 1 # fit on 5-days bins
    stepPredict = 5 # predict on (5*2=)10-days bins

    stddevs = [4,8,16,24]
    wsize = 64
    controls = applierControls(blockxsize=wsize, blockysize=wsize, wsize=wsize)
    job_landsat_ndvi_timeseries_rbf_gap_filling(indirname=dir_landsat, outdirname=dir_product,
                                                stddevs=stddevs,
                                                mgrsFootprints=mgrsFootprints,
                                                productname='timeseries', imagename='ndvi',
                                                dateRange=dateRange,
                                                stepFit=stepFit, stepPredict=stepPredict,
                                                controls=controls,
                                                processes=5, skip=False)

if __name__ == '__main__':
    from osgeo import gdal
    gdal.SetCacheMax(1)
    doit()
