from os.path import join
from lamos.datacube.types import MGRSFootprintCollection, WRS2FootprintCollection, LANDSAT_ANCHOR
from lamos.datacube.jobs import *
from hub.timing import tic, toc

def doit():

    rootDC = r'\\141.20.140.91\SAN_RSDBrazil\LandsatData\Landsat_Turkey\02_OutputData\LandsatX\datacube'
    dir_landsat = join(rootDC, 'landsat')
    dir_aux = join(rootDC, 'auxiliary')
    dir_landsatSample = join(rootDC, 'landsatSample')

    mgrsFootprints = MGRSFootprintCollection(['37SEB'])

    resolution = 30
    buffer = 300
    anchor = LANDSAT_ANCHOR

    shapefile = r'M:\LandsatData\Landsat_Turkey\02_OutputData\LandsatX\ar2_sample_pixels\37SEB_n25.shp'
    shapefile = r't:\test.shp'

    job_import_shapefile_as_mask(shapefile=shapefile,
                                 outdirname=dir_aux,
                                 productname='n25', imagename='mask',
                                 mgrsFootprints=mgrsFootprints,
                                 resolution=resolution, buffer=buffer, anchor=anchor,
                                 processes=1, skip=0)

if __name__ == '__main__':

    tic()
    doit()
    toc()
