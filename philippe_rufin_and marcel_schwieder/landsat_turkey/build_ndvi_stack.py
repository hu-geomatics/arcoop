from os.path import join
from lamos.datacube.types import MGRSFootprintCollection, WRS2FootprintCollection, LANDSAT_ANCHOR
from lamos.datacube.applier import applierControls
import lamos.datacube.jobs as jobs
from hub.timing import tic, toc

def doit():

    wrs2Footprints = WRS2FootprintCollection(['173034','172034'])
    mgrsFootprints = MGRSFootprintCollection(['37SEB'])

    dir_landsatWRS2 = r'\\141.20.140.91\SAN_RSDBrazil\LandsatData\Landsat_Turkey\02_OutputData\LandsatX\04_Landsat'
    dir_landsat = r'\\141.20.140.91\SAN_RSDBrazil\LandsatData\Landsat_Turkey\02_OutputData\LandsatX\datacube\landsat'
    dir_products = r'\\141.20.140.91\SAN_RSDBrazil\LandsatData\Landsat_Turkey\02_OutputData\LandsatX\datacube\products'

    resolution = 30
    buffer = 300
    anchor = LANDSAT_ANCHOR

    # import landsat scenes into data cube
    jobs.job_import_landsat_archive(indirname=dir_landsatWRS2, outdirname=dir_landsat,
                               wrs2Footprints=wrs2Footprints,
                               mgrsFootprints=mgrsFootprints,
                               resolution=resolution, buffer=buffer, anchor=anchor,
                               saveAsGTiff=True,
                               processes=50, skip=1)

    jobs.job_landsat_ndvi_timeseries(indirname=dir_landsat, outdirname=dir_products,
                                     mgrsFootprints=mgrsFootprints,
                                     productname='timeseries', imagename='ndvi_test2',
                                     processes=10, skip=0)

if __name__ == '__main__':

    import gdal
    gdal.SetCacheMax(1)  # MB

    tic()
    doit()
    toc()
