import numpy
import gdal
from os.path import join
import matplotlib.pyplot as plt
from hub.gdal.api import GDALMeta


def getArray(filename):
    ds = gdal.Open(filename)
    return ds.ReadAsArray()

def getDates(filename):
    ds = gdal.Open(filename)
    tmp = ds.GetMetadataItem('wavelength', 'ENVI').strip()
    dates = map(float, tmp[1:-1].split(','))
    return dates

def getNames(filename):
    ds = gdal.Open(filename)
    tmp = ds.GetMetadataItem('band names', 'ENVI')
    names = str(tmp)
    return [ds.GetRasterBand(b).GetDescription() for b in range(ds.GetRasterCount)]

def getNames(filename):
    ds = gdal.Open(filename)
    names = [ds.GetRasterBand(b).GetDescription() for b in range(1,(ds.RasterCount+1),1)]
    return names




#def script():

# root_dir = r'G:\_EnMAP\Rohdaten\Brazil\Raster\LANDSAT\datacubePETR\productMGRS_PETR\23L\LE\timeseries'
VegIndex = 'ndvi'
root_dir = r'M:\LandsatData\Landsat_Turkey\02_OutputData\LandsatX\datacube\products\36S\YG\timeseries'
stepA = '_step_1'
stepAB = '_step_1_5'
stddevs = [8, 16, 24]

outname = join(root_dir, VegIndex + '_' +str(stddevs[0]) + '_' +str(stddevs[1])+ '_' +str(stddevs[2]) + '_ensemble.tif' )

# setup filenames
forig = join(root_dir, 'ndvi_orig' + stepA + '.tif')
ffits = [join(root_dir, 'ndvi_stddev_' + str(stddev) + stepAB + '.tif') for stddev in stddevs]
fcounts = [join(root_dir, 'ndvi_counts_' + str(stddev) + stepAB + '.tif') for stddev in stddevs]

# read dates
origDates = getDates(forig)
ensembleDates = getDates(ffits[0])

# read data
print(str(root_dir))
print("read datasets")
orig = getArray(forig)
fits = [getArray(ffit) for ffit in ffits]
counts = [getArray(fcount) for fcount in fcounts]
stdOne = fits[0]

# calculate normalized weights
print("calculate weights")
countSum = numpy.sum(counts, axis=0)
weights = [numpy.float32(count)/countSum for count in counts]

# calculate ensemble
print("calculate ensemble")
ensemble = numpy.sum([fit*weight for fit, weight in zip(fits, weights)], axis=0)
ensemble = ensemble.astype(numpy.int16)
GT = gdal.Open(forig).GetGeoTransform()
PROJ = gdal.Open(forig).GetProjection()
#bandNames = gdal.Open(stdOne).GetBandNames
bands, rows, cols = stdOne.shape

print("create empty file")
drvMemR = gdal.GetDriverByName('MEM')
ensemble_raster = drvMemR.Create('', cols, rows, bands, gdal.GDT_Int16)
ensemble_raster.SetProjection(PROJ)
ensemble_raster.SetGeoTransform(GT)

print("write bands")
for b in range(1, bands+1, 1):
    ensemble_raster.GetRasterBand(b).WriteArray(ensemble[b-1,:,:])


driver = gdal.GetDriverByName("GTiff")
copy_ds = driver.CreateCopy(outname, ensemble_raster, 0)
copy_ds = None


# write Meta data file

bandnames = getNames(ffits[0])

wavelength = ensembleDates
outmeta = GDALMeta(outname)
outmeta.setNoDataValue(numpy.iinfo(numpy.int16).min)
outmeta.setMetadataItem('wavelength', wavelength)
outmeta.setMetadataItem('band names', bandnames)
outmeta.writeMeta()