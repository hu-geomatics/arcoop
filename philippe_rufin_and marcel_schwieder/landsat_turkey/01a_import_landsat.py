from os.path import join
from lamos.datacube.types import MGRSFootprintCollection, WRS2FootprintCollection, LANDSAT_ANCHOR
from lamos.datacube.jobs import *
from hub.timing import tic, toc
from lamos.datacube.applier import applierControls

def doit():

    root = r'\\141.20.140.91\SAN_RSDBrazil\LandsatData\Landsat_Turkey\02_OutputData\LandsatX'
    rootDC = join(root, 'datacube')
    dir_landsatWRS2 = join(root, '04_Landsat')
    dir_landsat = join(rootDC, 'landsat')
    dir_landsatSample = join(rootDC, 'landsatSample')
    dir_products = join(rootDC, 'products')
    dir_productSample = join(rootDC, 'productsSample')

    resolution = 30
    buffer = 300
    anchor = LANDSAT_ANCHOR

    wrs2Footprints = WRS2FootprintCollection(['173034','172034'])
    mgrsFootprints = MGRSFootprintCollection(['37SEB'])

    # import landsat scenes into data cube
    job_import_landsat_archive(indirname=dir_landsatWRS2, outdirname=dir_landsat,
                               wrs2Footprints=wrs2Footprints,
                               mgrsFootprints=mgrsFootprints,
                               resolution=resolution, buffer=buffer, anchor=anchor,
                               saveAsGTiff=True,
                               processes=50, skip=0)

    job_landsat_data_availability(indirname=dir_landsat, outdirname=dir_products,
                                  mgrsFootprints=mgrsFootprints,
                                  productname='statistics',
                                  imagename='counts',
                                  processes=50, skip=1)


if __name__ == '__main__':

    tic()
    doit()
    toc()