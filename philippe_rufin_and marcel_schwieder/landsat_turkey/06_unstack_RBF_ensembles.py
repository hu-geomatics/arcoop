import os
import gdal
from os.path import join

VegIndex = 'ndvi'
root_dir = r'M:\LandsatData\Landsat_Turkey\02_OutputData\LandsatX\datacube\products\38T\MK\timeseries'
stepA = '_step_1'
stepAB = '_step_1_5'
stddevs = [8, 16, 24]

outname = join(root_dir, VegIndex + '_' +str(stddevs[0]) + '_' +str(stddevs[1])+ '_' +str(stddevs[2]) + '_ensemble.tif' )
bands = gdal.Open(outname).RasterCount

for band in range(1, bands+1, 1):
    outband = join(root_dir, 'bands', VegIndex + '_' +str(stddevs[0]) + '_' +str(stddevs[1])+ '_' +str(stddevs[2]) + '_ensemble_band_' + str(band).zfill(3) + '.bsq' )
    os.system("gdal_translate -of ENVI -b " + str(band) + " " + outname + " " +  outband)