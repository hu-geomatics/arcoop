from __future__ import print_function
import inspect
from itertools import izip
from datetime import date, timedelta
from calendar import isleap
from multiprocessing.pool import ThreadPool
from osgeo import gdal, ogr
from numpy import float32, int16, full, nan, isfinite, nan_to_num, iinfo, any, less_equal, greater, cumsum, seterr, errstate, nanstd, abs
from astropy.convolution import convolve
from astropy.convolution.kernels import Gaussian1DKernel

from hubdc import Applier, ApplierOperator
from hubdc.gis.mgrs import getMGRSPixelGridsByShape
from hubdc.gis.wrs2 import getWRS2NamesInsidePixelGrid
from hubdc.landsat.LandsatCollection1ArchiveParser import LandsatCollection1ArchiveParser

LANDSAT_ANCHOR = (15, 15)

def getShapefileGeometry(shapefile):
    ds = ogr.Open(shapefile)
    layer = ds.GetLayer()
    geometry = ogr.Geometry(ogr.wkbMultiPolygon)
    for feature in layer:
        geometry.AddGeometry(feature.GetGeometryRef())
    return geometry.UnionCascaded()

def runROI():

    debug = 1
    if not debug:
        napplier = 1
        nworker = 1
        nwriter = 1
    else:
        napplier = None
        nworker = None
        nwriter = None

    if napplier > 0:
        pool = ThreadPool(processes=napplier)

    #landsatArchive = r'C:\Work\data\marcel\collection1'
    #output = r'c:\Work\outputs\fit{mgrsFootprint}.img'
    #roi = getShapefileGeometry(r'C:\Work\data\marcel\roi.shp')

    landsatArchive = r'G:\_EnMAP\Rohdaten\Brazil\Raster\LANDSAT\ESPA\PESA\Collection_1\EVI'
    output = r'G:\_EnMAP\Rohdaten\Brazil\Raster\LANDSAT\ESPA\PESA\Collection_1\ar_outputs\fit__{mgrsFootprint}.img'
    roi = getShapefileGeometry(r'G:\_EnMAP\Rohdaten\Brazil\Raster\LANDSAT\ESPA\PESA\Collection_1\roi.shp')

    for mgrsFootprint, grid in getMGRSPixelGridsByShape(shape=roi, res=3000, anchor=LANDSAT_ANCHOR, pixelBuffer=1, trim=True):
        if napplier is None:
            runFootprint(mgrsFootprint, grid, landsatArchive, output, nworker, nwriter)
        else:
            pool.apply_async(func=runFootprint, args=(mgrsFootprint, grid, landsatArchive, output, nworker, nwriter))

    if napplier > 0:
        pool.close()
        pool.join()

def ndvi(nir, red):
    return float32(nir - red) / float32(nir + red)

def evi(evi):
    return evi / float32(10000)

def runFootprint(mgrsFootprint, grid, landsatArchive, output, nworker, nwriter):

    wrs2Footprints = getWRS2NamesInsidePixelGrid(grid=grid)

    applier = Applier()
    names = ['cfmask', 'evi']
    mtlFiles, imageFiles = LandsatCollection1ArchiveParser.getFilenames(archive=landsatArchive, footprints=wrs2Footprints, names=names)
    for i, name in enumerate(names):
        applier.setInputList(name, filenames=imageFiles[i], resampleAlg=gdal.GRA_NearestNeighbour, errorThreshold=0., warpMemoryLimit=1000 * 2 ** 20, multithread=True)
    applier.setOutput('fit', filename=output.format(mgrsFootprint=mgrsFootprint),
                      #format='ENVI',
                      format='GTiff',
                      #creationOptions=[])
                      creationOptions=['COMPRESS=LZW', 'INTERLEAVE=BAND', 'TILED=YES', 'BLOCKXSIZE=256',
                                       'BLOCKYSIZE=256', 'SPARSE_OK=TRUE'])#, 'NUM_THREADS=ALL_CPUS', 'BIGTIFF=YES'])


    applier.controls.setReferenceGrid(grid)
    applier.controls.setNumThreads(nworker)
    applier.controls.setNumWriter(nwriter)
    applier.controls.setWindowXSize(256)
    applier.controls.setWindowYSize(256)
    #applier.controls.setWindowFullSize()

    description = ' {mgrsFootprint}[{xsize}x{ysize}], {nscenes} Scenes in WRS2 [{wrs2Footprints}]'.format(xsize=grid.getDimensions()[1], ysize=grid.getDimensions()[0], mgrsFootprint=mgrsFootprint, nscenes=len(mtlFiles), wrs2Footprints=', '.join(wrs2Footprints))
    applier.apply(operator=RBFFitter, description=description,
                  fIndex=evi, mtlFiles=mtlFiles,
                  start=date(2013, 2, 16), end=date(2015, 12, 31),
                  workTempRes=8, outTempRes=8,
                  sigmas=[7, 15, 31], sigmaCutOff=0.9,
                  outlierDetection=True, outlierStd=1, outlierSigma=31)


class RBFFitter(ApplierOperator):

    def ufunc(self, fIndex, mtlFiles, start, end, workTempRes, outTempRes,
              sigmas, sigmaCutOff=0.9,
              outlierDetection=False, outlierStd=1, outlierSigma=30,
              dtype=float32, noData=nan, scale=None):

        if self.isFirstBlock():
            if outTempRes % workTempRes != 0:
                raise ValueError('output resolution must by a multiple of working resolution')

        self.parseAcquisitionDatesFromMTLFiles(mtlFiles)
        ts = self.createEvenlySpacedTemporalGrid(fIndex, start, end, workTempRes)
        if outlierDetection:
            self.excludeOutliers(ts, outlierSigma, outlierStd, sigmaCutOff, workTempRes)
        da = self.calculateDataAvailability(ts)
        fit = self.calculateRBFEnsembleFit(ts, da, sigmas, sigmaCutOff, workTempRes)
        self.setArray('fit', array=fit[::outTempRes//workTempRes], replace=[nan, noData], scale=scale, dtype=dtype)

        if self.isLastBlock():
            self.setMetadata(start, end, outTempRes)

        return

    def setMetadata(self, start, end, outTempRes):
        def decimalYear(d):
            doy = (d - date(d.year, 1, 1)).days + 1
            dyear = d.year + doy / (365. + isleap(d.year))
            return dyear

        bandNames = list()
        wavelength = list()
        for days in range((end - start).days + 1):
            d = start + timedelta(days=days)
            bandNames.append(str(d))
            wavelength.append(decimalYear(d))
        self.setMetadataItem('fit', key='band names', value=bandNames[::outTempRes], domain='ENVI')
        self.setMetadataItem('fit', key='wavelength', value=wavelength[::outTempRes], domain='ENVI')

    def calculateRBFEnsembleFit(self, ts, da, sigmas, sigmaCutOff, workTempRes):
        fit = 0.
        weightTotal = 0.
        for i, sigma in enumerate(sigmas):
            kernel = self.getKernel(sigma=sigma, workTempRes=workTempRes, kernelCutOff=sigmaCutOff)

            weight = self.filterDataAvailability(da=da, kernel=kernel)
            fit += nan_to_num(self.filterTimeseries(ts=ts, kernel=kernel)) * weight
            weightTotal += weight
        oldErr = seterr(all='ignore')
        fit /= weightTotal
        seterr(**oldErr)
        return fit

    def calculateDataAvailability(self, ts):
        da = isfinite(ts).astype(float32)  # need to convert to float32, otherwise convolve will produce a float64
        return da

    def excludeOutliers(self, ts, outlierSigma, outlierStd, sigmaCutOff, workTempRes):
        kernel = self.getKernel(sigma=outlierSigma, workTempRes=workTempRes, kernelCutOff=sigmaCutOff)
        fit = self.filterTimeseries(ts, kernel)
        std = nanstd(fit, axis=0)
        for i, (its, ifit) in enumerate(izip(fit, ts)):
            error = abs(ifit - its)
            oldErr = seterr(all='ignore')
            invalid = error / std < outlierStd
            seterr(**oldErr)
            its[invalid] = nan

    def createEvenlySpacedTemporalGrid(self, fIndex, start, end, workTempRes):

        # arrange irregularly distributed obvervations into a evenly spaced temporal grid
        ysize, xsize = self.grid.getDimensions()
        bands = (end - start).days // workTempRes + 1
        ts = full((bands, ysize, xsize), fill_value=nan, dtype=float32)
        argNames = inspect.getargspec(fIndex)[0]
        for i, acqDate in enumerate(self.acquisitionDates):
            cfmask = self.getArray(('cfmask', i))
            args = [self.getArray((argName, i)) for argName in argNames]
            valid = less_equal(cfmask, 1)  # use clear land and clear water observations
            if not any(valid): continue
            index = (acqDate - start).days // workTempRes
            if index < 0 or index > bands - 1: continue
            ts[index][valid[0]] = fIndex(*(arg[valid] for arg in args))

        return ts

    def parseAcquisitionDatesFromMTLFiles(self, mtlFiles):

        if 'acquisitionDates' not in self.__dict__:
            self.acquisitionDates = list()
            for mtlFile in mtlFiles:
                with open(mtlFile) as f:
                    while True:
                        line = f.readline().strip()
                        if line.startswith('DATE_ACQUIRED'):
                            self.acquisitionDates.append(date(year=int(line[-10:-6]), month=int(line[-5:-3]), day=int(line[-2:])))
                            break

    def filterTimeseries(self, ts, kernel):
        return convolve(array=ts, kernel=kernel, boundary='fill', fill_value=nan, normalize_kernel=True)

    def filterDataAvailability(self, da, kernel):
        return convolve(da, kernel, boundary='fill', fill_value=0, normalize_kernel=True)

    def getKernel(self, sigma, workTempRes, kernelCutOff):

        sigma = float(sigma) / workTempRes
        alpha = 1-kernelCutOff
        kernel = Gaussian1DKernel(sigma).array
        kernelCumSum = cumsum(kernel.flatten())
        mask = kernelCumSum > alpha/2
        mask *= mask[::-1]
        return kernel[mask].reshape((-1, 1, 1))

if __name__ == '__main__':

    from timeit import default_timer
    t0 = default_timer()
    runROI()
    s = (default_timer() - t0); m = s / 60; h = m / 60
    print('done ROI Processing in {s} sec | {m}  min | {h} hours'.format(s=int(s), m=round(m, 2), h=round(h, 2)))
