import numpy as np
import gdal
import matplotlib.pyplot as plt
import astropy


#root_dir = 'Y:/DAR_Data/01_Processing/Processed_Data/LandsatX/productSample3/37S/EB/timeseries/'
root_dir = 'G:/_EnMAP/Rohdaten/Brazil/Raster/LANDSAT/datacubePETR/productSample3/22K/HH/timeseries/'
#########################################################################
#########################################################################
### read in data

NDVI = gdal.Open(root_dir + 'ndvi_orig.tif')
NDVI_array = NDVI.ReadAsArray()

RBF_fit_matrix = []
RBF_count_matrix = []

for n in range(1, 51, 1):
    RBF_fit = gdal.Open(root_dir + 'ndvi_stddev_' + str(n) + '.tif')
    RBF_count = gdal.Open(root_dir + 'ndvi_counts_' + str(n) + '.tif')

    RBF_fit_matrix.append(RBF_fit.ReadAsArray())
    RBF_count_matrix.append(RBF_count.ReadAsArray())

#########################################################################
#########################################################################
### normalize weights across counts

RBF_count_kernel_sum = np.nansum(np.array(RBF_count_matrix), axis = 0)
RBF_count_kernel_sum = np.expand_dims(RBF_count_kernel_sum, 0)

RBF_count_matrix = np.array(RBF_count_matrix)
RBF_norm_count_matrix = RBF_count_matrix / RBF_count_kernel_sum

### calculate density-weighted RBF fit

RBF_weighted = np.array(RBF_fit_matrix) * np.array(RBF_norm_count_matrix )
print(RBF_weighted.shape)

RBF_average = np.nanmean(RBF_weighted, axis=0)
print(RBF_average.shape)
print(NDVI_array.shape)

#########################################################################
#########################################################################
### compare with original NDVI

NDVI_index = np.where(NDVI_array >= 0)
np.isnan(NDVI_array)

NDVI_sample = NDVI_array[-np.isnan(NDVI_array)]
RBF_sample = RBF_average[-np.isnan(NDVI_array)]

NDVI_RBF_residuals = NDVI_sample - RBF_sample

def rmse(predictions, targets):
    return np.sqrt(((predictions - targets) ** 2).mean())

RMSE = rmse(RBF_sample, NDVI_sample)
print(RMSE)